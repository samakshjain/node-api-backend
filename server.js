var mysql      = require('mysql');
var express    = require('express');
var bodyParser = require ('body-parser');
var validateRequest =  require('./middlewares/validateRequest');
var app        = express();
var morgan     = require('morgan');

//inits to parse request body as json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));

app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

app.all('/api/v1/*',  validateRequest);
app.use('/', require('./routes'));

app.use(function(req,res, next){
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

app.set('port', process.env.PORT || 3000 );

var server = app.listen(app.get('port'), function(){
	console.log('Express server listening at port ' + server.address().port);
});

//EOF!
