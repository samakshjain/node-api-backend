var connection = require('../dbConnection');
var auth = require('../routes/auth');
var validateRequest = function(req, res, next) {
 
  // When performing a cross domain request, you will recieve
  // a preflighted request first. This is to check if our the app
  // is safe.
 
  // We skip the token outh for [OPTIONS] requests.
  //if(req.method == 'OPTIONS') next();
 
  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
  if (token) {
     // Authorize the user to see if s/he can access our resources 
        auth.validateUser(token, function(data){
        if (data) {
          if ((req.url.indexOf('admin') >= 0 && data.role == 'admin') || (req.url.indexOf('admin') < 0 && req.url.indexOf('/api/v1/') >= 0)) {
           next(); // To move to next middleware
          } else {
          res.status(403);
          res.json({
            "status": 403,
            "message": "Not Authorized"
          });
          //return;
          }
       } else {
        // No user with this name exists, respond back with a 401
        res.status(401);
        res.json({
          "status": 401,
          "message": "Invalid User!"
        });
        //return;
      }
      }); // The key would be the logged in user's username

  } else {
    res.status(401);
    res.json({
      "status": 401,
      "message": "Invalid Token or Key"
    });
    return;
  }
};

module.exports = validateRequest;