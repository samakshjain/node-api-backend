var connection = require('../dbConnection');
var moment =  require ('moment');

var users = {
 
  getAll: function(req, res) {
    connection.query('SELECT * from `users` INNER JOIN `user_details` ON users.id=user_details.id', function(err, rows, fields) {
          if (!err) {
            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.end(JSON.stringify(rows,null, '\t')); 
          }
          else{
            console.log(err);
            res.status(400);
           res.end("Some Err");
          }
      });
  },
 
  getOne: function(req, res) {
    var id = req.params.id;
    var q_id = "SELECT * FROM `users` WHERE `id`=" + connection.escape(id);
    connection.query(q_id, function(err, rows, fields){
      if (!err)
        res.end(JSON.stringify(rows,null, '\t')); 
      else 
       res.json("No auctions of id" + id);
    });
  },
 
  create: function(req, res) {
    req.body["updated_at"] = moment().utc().format(); 
    connection.query("INSERT INTO `users` SET ?", req.body,function(err, rows){
      if(!err){
        connection.query("INSERT INTO `user_details` SET `id` =" + rows.insertId);
        res.status(201);
        res.end(JSON.stringify(rows,null, '\t'));
      }
      else 
        res.status(400);
      });
  },
 
  block: function(req, res) {
    connection.query("UPDATE `users` SET `banned` = 1 WHERE `id` =" + req.params.id, function(err,row){
      if(!err){
        res.status(202);
        res.json(row);
      }
      else
        res.status(400);
        res.end("Invalid req.");
    });
  },
 
  delete: function(req, res) {
    connection.query("DELETE FROM `users` WHERE `id` =" + req.params.id, function(err, results){
      if(!err){
        connection.query("DELETE FROM `user_details` WHERE `id` =" + req.params.id);
        res.status(204);
        res.end("Deleted Successfully!");
      } else {
        res.status(400);
        res.end("Invalid request.");
      }
    });
  },
  update : function(req,res){
    req.body["updated_at"] = moment().utc().format();
    connection.query("UPDATE `users` SET ? WHERE `id` =" + req.params.id, req.body, function(err,row){
      if(!err){
        res.status(202);
        res.json(row);
      }
      else
        res.status(400);
        res.end("Invalid req.");
    });
  },

  addPoints : function(req,res){
    connection.query("");
  },

  upcoupon : function(req,res){
    connection.query("");
  },
  setStatus : function(req,res){
    q = "UPDATE `users` SET `active_flag`=" + req.body['status'] + " WHERE `id` in ("+ req.body['ids'].toString() +")";
    qcallb = function(err,results){
      if(!err){
        res.end(JSON.stringify(results,null, '\t')); 
      } else {
        res.status(400);
        res.json(err);
      }
    }
    connection.query(q,qcallb)
  },
  getScoreLog : function(req,res){
    q = "SELECT `updated_score`,`activity` FROM `user_scores` WHERE `user_id` =" + req.params.id;
    qcallb = function(err, rows){
      if(!err){
        res.writeHead(200, {'Content-Type' : 'text/plain'});
        res.end(JSON.stringify(rows,null, '\t')); 
      } else {
        res.status(400);
        res.json(err);
      }
    }
    connection.query(q, qcallb)
  },
  getFavs : function(req,res){
    q = "SELECT `coupon_id`,`mission_id`,`mission_id` FROM `user_favs` WHERE `user_id` =" + req.params.id;
    qcallb = function(err, rows){
      if(!err){
        res.writeHead(200, {'Content-Type' : 'text/plain'});
        res.end(JSON.stringify(rows,null, '\t')); 
      } else {
        res.status(400);
        res.json(err);
      }
    }
    connection.query(q, qcallb)
  },
  getInvitations : function(req,res){
    q = "SELECT `invitation_email`,`invitation_type` FROM `user_invitations` WHERE `user_id` =" + req.params.id;
    qcallb = function(err, rows){
      if(!err){
        res.writeHead(200, {'Content-Type' : 'text/plain'});
        res.end(JSON.stringify(rows,null, '\t')); 
      } else {
        res.status(400);
        res.json(err);
      }
    }
    connection.query(q, qcallb)
  },
  getAuctions : function(req,res){
    q = "SELECT `bid` FROM `user_auctions` WHERE `user_id` =" + req.params.id;
    qcallb = function(err, rows){
      if(!err){
        res.writeHead(200, {'Content-Type' : 'text/plain'});
        res.end(JSON.stringify(rows,null, '\t')); 
      } else {
        res.status(400);
        res.json(err);
      }
    }
    connection.query(q, qcallb) 
  },
invites: function(req, res) {
   req.body["user_id"]= req.params.id;
   qcallb = function(err, results){
     if(!err){
      res.writeHead(201, {'Content-Type' : 'text/plain'});
      res.end(JSON.stringify(results,null, '\t'));
     } else {
      res.status(400);
      res.json(err);
     }
   }
   connection.query("INSERT INTO `user_invitations` SET ?", req.body, qcallb);
  }
};
 
module.exports = users;