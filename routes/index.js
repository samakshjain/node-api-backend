var express = require('express');
var router = express.Router();

/*
*Externalize each route
*/
var auth = require('./auth.js');
var coupons = require('./coupons.js');
var users = require('./users.js');
var missions = require('./missions.js');
var auctions = require('./auctions.js');
var categories = require('./categories.js');
var brands = require('./brand.js');
var states =  require('./states.js')

/*
* Routes that can be accessed by any one
*/
router.post('/register',auth.register);
router.post('/login', auth.login);
router.post('/logout', auth.logout);
router.get('/coupons', coupons.getAll);
router.get('/categories', categories.getAll);
router.get('/createcoupon', coupons.create);

/*
* Routes that can be accessed only by autheticated users
*/
router.get('/api/v1/coupons', coupons.getAll);
router.get('/api/v1/coupon/:id', coupons.getOne);
router.post('/api/v1/coupon/', coupons.create);
router.put('/api/v1/coupon/:id', coupons.update);
router.delete('/api/v1/coupon/:id', coupons.delete);
router.post('/api/v1/user', users.create);
router.post('/api/v1/user/upcoupon', users.upcoupon);
router.post('/api/v1/user/:id/invites', users.invites)

/*
* Routes that can be accessed only by authenticated & authorized users *admins*
*/

/*==========  CRUD+ for users  ==========*/

router.get('/api/v1/admin/users', users.getAll);
router.get('/api/v1/admin/user/:id', users.getOne);
router.post('/api/v1/admin/user/', users.create);
router.put('/api/v1/admin/user/:id', users.update);
router.delete('/api/v1/admin/user/:id', users.delete);
router.put('/api/v1/admin/user/:id/block', users.block);
router.put('/api/v1/admin/users/setStatus/', users.setStatus);
router.get('/api/v1/admin/user/:id/getFavs', users.getFavs);
router.get('/api/v1/admin/user/:id/getScoreLog', users.getScoreLog);
router.get('/api/v1/admin/user/:id/getInvitations', users.getInvitations);
router.get('/api/v1/admin/user/:id/getAuctions', users.getAuctions);

/*==========  CRUD+ for coupons  ==========*/

router.get('/api/v1/admin/coupons/', coupons.getAll);
router.get('/api/v1/admin/coupon/:id', coupons.getOne);
router.post('/api/v1/admin/coupon/', coupons.create);
router.put('/api/v1/admin/coupon/:id', coupons.update);
router.delete('/api/v1/admin/coupon/:id', coupons.delete);
router.get('/api/v1/admin/coupon/:id/categories', coupons.getCat);
router.post('/api/v1/admin/coupon/:id/category', coupons.addCat);
router.delete('/api/v1/admin/coupon/:id/category', coupons.deleteCat)
router.put('/api/v1/admin/coupons/setStatus/', coupons.setStatus);
router.get('/api/v1/admin/coupon/:id/getLog', coupons.getLog);

/*==========  CRUD+ for missions  ==========*/

router.get('/api/v1/admin/missions', missions.getAll);
router.get('/api/v1/admin/mission/:id', missions.getOne);
router.post('/api/v1/admin/mission/', missions.create);
router.put('/api/v1/admin/mission/:id', missions.update);
router.delete('/api/v1/admin/mission/:id', missions.delete);
router.get('/api/v1/admin/mission/:id/getParticipants', missions.getParticipants);

/*==========  CRUD+ for auctions  ==========*/

router.get('/api/v1/admin/auctions', auctions.getAll);
router.get('/api/v1/admin/auction/:id', auctions.getOne);
router.post('/api/v1/admin/auction/', auctions.create);
router.put('/api/v1/admin/auction/:id', auctions.update);
router.delete('/api/v1/admin/auction/:id/', auctions.delete);
//router.put('/api/v1/admin/auction/:id/togglelive', auctions.togglelive);
router.get('/api/v1/admin/auction/:id/getBids', auctions.getBids);

/*==========  CRUD+ for categories  ==========*/

router.get('/api/v1/admin/categories', categories.getAll);
router.get('/api/v1/admin/categories/:id', categories.getOne);
router.post('/api/v1/admin/categories/', categories.create);
router.put('/api/v1/admin/categories/:id', categories.update);
router.delete('/api/v1/admin/categories/:id', categories.delete);

/*==========  Brand API  ==========*/

router.get('/api/v1/admin/brands', brands.getAll);

/*==========  State API  ==========*/

router.get('/api/v1/admin/states', states.get);
router.post('/api/v1/admin/states', states.post);

/*==========  Exports  ==========*/

module.exports = router;