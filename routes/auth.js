var tokenizer  = require('rand-token');
var connection = require('../dbConnection');
var moment =  require ('moment');

var auth = {
 
  login: function(req, res) {
    var username = req.body.username || '';
    var password = req.body.password || '';
    var dbUserObj= '';
    
    if (username == '' || password == '') {
      res.sendStatus(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
      return;
    }   
    // // Fire a query to your DB and check if the credentials are valid
    auth.validate(username, password, function(data){
      if (!data || data == 401) { // If authentication fails, we send a 401 back
      res.status(401);
      res.json({
        "status": 401,
        "message": "Invalid credentials"
      });
      return;
    } else {
      // If authentication is success, we will generate a token
      // and dispatch it to the client
      res.send(data);
      res.status(200); 
    }
    });    
  },

  logout : function (req, res) {
      var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
      connection.query("DELETE FROM `admin_sessions` WHERE `token` = ?", [token], function(err, results){
        if(!err){
          res.end("Logged Out Successfully!")
        }
        else
          res.end(err);
      });
  },

  register : function (req,res){
    req.body['updated_at'] = moment().utc().format();
    connection.query("INSERT INTO `users` SET ?", req.body, function(err, result){
      if(!err){
        res.status(200);
        res.json(result);
      } else {
        res.status(400);
        res.json(err);
      }

    });

  },
 
  validate : function(username, password, success) {
    // spoofing the DB response for simplicity
    var sql = "SELECT * FROM `users` WHERE `username` = '" + username + "' and `password` = '" + password + "'"; 
    //checks for user name and pass validity
    connection.query(sql, function(err, rows, fields){
      if (rows.length!=0){
        //16 char token generated 
        var token = tokenizer.generate(16);
        var with_token = {
          id : rows[0].id,
          token : token,
          issued_at : moment().utc().format(),
          last_seen : moment().utc().format(),
          role : rows[0].role
        };
        
      //checks if user already in session by the result callback of the following query
      connection.query("INSERT INTO `admin_sessions` SET ?", with_token, function(err,results){
          if (err==null) {
            success(token);
          } else {
              //returns the value of the already generated token if admin already in session
              connection.query("SELECT * FROM `admin_sessions` WHERE `id` =" + with_token.id , function(err, rows, fields){
              if(!err){
              //  res.status(202);
                var last = {'last_seen':moment().utc().format()};
                connection.query("UPDATE `admin_sessions` SET ? WHERE `id` ="+ with_token.id, last);
                success(rows[0].token);
              } else
                success("Something is fishy, please contact support!"); //should never happen 
              }); 
          } 
        });        
      } else {
              success(401);
     }
    });
  },
 
 validateUser : function(token, success) {
   var sql = "SELECT * FROM `admin_sessions` WHERE `token` =" + "'" +token +"'";
   connection.query(sql, function(err, rows, fields){
    if (rows.length != 0){
      var last_seen = rows[0].last_seen;
      //48 hours timeout
      if (moment().utc().diff(last_seen,'seconds') <172800){
        var last = {"last_seen" : moment().utc().format()};
        connection.query("UPDATE `admin_sessions` SET ? WHERE `token` =" + "'" +token +"'", last);
        success(rows[0]);
      } else {
        connection.query("DELETE FROM `admin_sessions` WHERE `token` = ?", [token])
        success();
      }
    } else {
      success();
    }
   });
 }
};


    
 
module.exports = auth;