var connection = require('../dbConnection');
var moment =  require ('moment');

var brands = {
 
  getAll: function(req, res) {
    connection.query('SELECT * from `brands`', function(err, rows, fields) {
          if (!err) {
            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.end(JSON.stringify(rows,null, '\t')); 
          }
          else
           res.end("Some Err");
      });
  },
 
  getOne: function(req, res) {
    var id = req.params.id;
    var q_id = "SELECT * FROM `brands` WHERE `id`=" + connection.escape(id);
    connection.query(q_id, function(err, rows, fields){
      if (!err)
        res.end(JSON.stringify(rows,null, '\t')); 
      else 
       res.json("No brands of id" + id);
    });
  },
 
  create: function(req, res) {
    req.body["updated_at"] = moment().utc().format();
    connection.query("INSERT INTO `brands` SET ?", req.body,function(err, rows){
      if(!err)
        res.end(JSON.stringify(rows,null,  '\t'));
      else 
        res.status(400);
      });
  },
 
  update: function(req, res) {
    req.body["updated_at"] = moment().utc().format();
    connection.query("UPDATE `brands` SET ? WHERE `id` =" + req.params.id, req.body, function(err,row){
      if(!err){
        res.status(202);
        res.json(row);
      }
      else
        res.status(400);
        res.end("Invalid req.");
    });
  },
 
  delete: function(req, res) {
    connection.query("DELETE FROM `brands` WHERE `id` =" + req.params.id, function(err, results){
      if(!err){
        res.status(204);
        res.end("Deleted Successfully!");
      } else {
        res.status(400);
        res.end("Invalid request.");
      }
    });
  }
};

module.exports = brands;