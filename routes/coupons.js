var connection = require('../dbConnection');
var moment =  require ('moment');

var coupons = {
 
  getAll: function(req, res) {
    connection.query('SELECT coupons.*, brands.name from coupons LEFT JOIN brands ON brands.id = coupons.brand', function(err, rows, fields) {
          if (!err) {
            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.end(JSON.stringify(rows,null, '\t')); 
          }
          else
           res.end("Some Err");
        });
  },
 
  getOne: function(req, res) {
    var id = req.params.id;
    var q_id = "SELECT * FROM `coupons` WHERE `id`=" + connection.escape(id);
    connection.query(q_id, function(err, rows, fields){
      if (!err)
        res.end(JSON.stringify(rows,null, '\t')); 
      else 
       res.end("No coupons of id" + id);
    });
    
  },
 
  create: function(req, res) {
    req.body["coupon"]["updated_at"] = moment().utc().format();
    // var add_q =  + reqbody.coupon.id + ", '" + reqbody.coupon.name + "' , " + reqbody.coupon.price + ", '" + reqbody.coupon.category + "' , '" + reqbody.coupon.brand + "' , '" + reqbody.coupon.image + "' , '" + reqbody.coupon.valid_thru  + "')" ; 
    connection.query("INSERT INTO `coupons` SET ?", req.body['coupon'],function(err, results){
        if(!err) {
          res.end(JSON.stringify(results,null,  '\t'));
          cats = req.body["cat_ids"];
          for (i = 0; i < cats.length; i++) { 
              data = {};
              data["coupon_id"] = results.insertId;
              data["category_id"] = cats[i];
              connection.query("INSERT INTO `coupon_categories` SET ?", data, function(err, results){
                if(err){
                 //do nothing 
                }
              }); 
              q = "select name from categories right join coupon_categories on categories.id = coupon_categories.category_id where coupon_id = " + data["coupon_id"];
              connection.query(q, function(err,rows,fields){
                if(!err){
                  cat_names = [];
                  for (i=0; i <rows.length; i++){
                   cat_names.push(rows[i]["name"]);
                  };
                  cat_names = cat_names.join();
                  connection.query("UPDATE `coupons` SET `categories` = '"+cat_names.toString()+"' WHERE `id` = " + data["coupon_id"], function(err){
                    if (err){
                      //do nothing.
                    }
                  })
                }
              });
   
          };
        } else {
          res.status(400);
        }
      });
  },
 
  update: function(req, res) {
    req.body["updated_at"] = now =  moment().utc().format();
    connection.query("UPDATE `coupons` SET ? WHERE `id` =" + req.params.id, req.body, function(err,row,fields){
      if(!err){
        connection.query("select `username` from `users` right join `admin_sessions` on admin_sessions.id = users.id where `token`= '" + req.headers["x-access-token"] + "'", function(error,rows){
          if(!error){
              connection.query("INSERT INTO `log.coupons` SET `message` ='" + rows[0]['username'].toUpperCase() + " updated coupon ID = <" + req.params.id + "> at " + moment().format('MMMM Do YYYY, h:mm:ss a') +"'");  
            }
        });
        res.status(202);
        res.json(fields);
      } else{
        res.status(400);
        res.json([]);
      }
    });
  },
 
  delete: function(req, res) {
    connection.query("DELETE FROM `coupons` WHERE `id` =" + req.params.id, function(err, results){
      if(!err){
        res.status(204);
        res.end("Deleted Successfully!");
      } else {
        res.status(400);
        res.json([]);
      }
    });
  },
  addCat : function (req,res) {
    cats = req.body["cat_ids"];
    for (i = 0; i < cats.length; i++) { 
        data = {};
        data["coupon_id"] = req.params.id;
        data["category_id"] = cats[i];
        connection.query("INSERT INTO `coupon_categories` SET ?", data, function(err, results){
          if(err){
           //do nothing 
          }
        });    
    };
    res.send("updated");
    },
  deleteCat : function (req,res) {
    connection.query("DELETE FROM `coupon_categories` WHERE `coupon_id` = '" + req.params.id + "' AND `category_id` = '" + req.body.category_id + "'", function(err,results){
      if(!err){
        res.status(204);
        res.json({});
      } else {
        res.status(400);
        res.json(err);
      }
    });
  },
  getCat: function(req,res){
    qcallb = function(err,rows, fields){
      if (!err){
        res.end(JSON.stringify(rows,null, '\t'));
      } else {
        res.status(400);
        res.json(err);
      }
    }
    q = "select name, categories.id from categories right join coupon_categories on categories.id = coupon_categories.category_id where coupon_id = " + req.params.id;
    connection.query(q, qcallb);
  },
  setStatus : function(req,res){
    q = "UPDATE `coupons` SET `active_flag`=" + req.body['status'] + " WHERE `id` in ("+ req.body['ids'].toString() +")";
    qcallb = function(err,results){
      if(!err){
        res.end(JSON.stringify(results,null, '\t')); 
      } else {
        res.status(400);
        res.json(err);
      }
    }
    connection.query(q,qcallb)
  },
  getLog : function(req,res){
    q = "SELECT `message` FROM `log.coupons` WHERE `coupon_id` =" + req.params.id;
    qcallb = function(err, rows){
      if (!err){
        res.end(JSON.stringify(rows,null, '\t')); 
      } else {
        res.end(400);
      }
    }
    connection.query(q,qcallb)
  }
};

module.exports = coupons;