var connection = require('../dbConnection');
var moment =  require ('moment');

var auctions = {
 
  getAll: function(req, res) {
    connection.query('SELECT * from auctions', function(err, rows, fields) {
          if (!err) {
            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.end(JSON.stringify(rows,null, '\t')); 
          }
          else
           res.end("Some Err");
      });
  },
 
  getOne: function(req, res) {
    var id = req.params.id;
    var q_id = "SELECT * FROM `auctions` WHERE `id`=" + connection.escape(id);
    connection.query(q_id, function(err, rows, fields){
      if (!err)
        res.end(JSON.stringify(rows,null, '\t')); 
      else 
       res.json("No auctions of id" + id);
    });
  },
 
  create: function(req, res) {
    req.body["updated_at"] = moment().utc().format();
    
    connection.query("INSERT INTO `auctions` SET ?", req.body,function(err, rows){
      if(!err)
        res.end(JSON.stringify(rows,null,  '\t'));
      else 
        res.status(400);
      });
  },
 
  update: function(req, res) {
    req.body["updated_at"] = moment().utc().format();

    connection.query("UPDATE `auctions` SET ? WHERE `id` =" + req.params.id, req.body, function(err,row){
      if(!err){
        res.status(202);
        res.json(row);
      }
      else
        res.status(400);
        res.end("Invalid req.");
    });
  },
 
  delete: function(req, res) {
    connection.query("DELETE FROM `auctions` WHERE `id` =" + req.params.id, function(err, results){
      if(!err){
        res.status(204);
        res.end("Deleted Successfully!");
      } else {
        res.status(400);
        res.end("Invalid request.");
      }
    });
  },

  togglelive : function(req,res){
    var q = "UPDATE `auctions` SET `live` ="+ !req.body.live+ " WHERE `id` ="+req.params.id;
    console.log(q);
    connection.query(q, function(err, result){
      if(!err){
        // console.log(query);
        res.status(200);
        res.json(result.message);
      } else {
        res.status(400);
      }
    });
  },

  suspend : function(req,res){
    connection.query("UPDATE `auctions` SET `live`=0 WHERE `id` =" + req.params.id, function(err, result){
      if(!err){
        res.status(200);
        res.json(result);
      } else {
        res.status(400);
      }
    });
  },

  getBids: function(req,res){
    connection.query('SELECT * FROM `auction_bids` WHERE `auction_id`=' + req.params.id, function(err, rows){
      if(!err){
        res.writeHead(200, {'Content-Type' : 'text/plain'});
        res.end(JSON.stringify(rows,null, '\t'));
      } else {
        res.status(400);
      }
    })
  }
};

module.exports = auctions;