var connection = require('../dbConnection');
var moment =  require ('moment');
var states = {
 
  get: function(req, res) {
    connection.query('SELECT `name` from states', function(err, rows, fields) {
          if (!err) {
            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.end(JSON.stringify(rows,null, '\t')); 
          }
          else
           res.end("Some Err");
        });
  },
  
  post: function(req, res) {
    connection.query("INSERT INTO `states` SET ?", req.body, function(err, rows){
      if(!err)
        res.end(JSON.stringify(rows,null, '\t'));
      else 
        res.status(400);
      });
  }
};
 
module.exports = states;